package logica;

public class Cuenta {
	private int numero;
	private int saldo;
	private String tipo;
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Cuenta(int numero, int saldo, String tipo) {
		this.numero = numero;
		this.saldo = saldo;
		this.tipo = tipo;
	}
	
	public Cuenta(int numero) {
		this.numero = numero;
		this.saldo = 0;
		this.tipo = "Ahorros";
	}
	
	public void consignar(int valor) {
		this.saldo += valor;
	}
	
	public void retirar(int valor) throws ExcepcionPersonalizada  {
		if(valor > this.saldo) {
			throw new ExcepcionPersonalizada("Saldo insuficiente");
		}else {
			this.saldo -= valor;
		}
	}
	
	public void transferir(Cuenta cuentaDestino, int valor) throws ExcepcionPersonalizada {
		this.retirar(valor);
		cuentaDestino.consignar(valor);
	}
	
}
