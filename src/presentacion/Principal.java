package presentacion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import logica.Cuenta;
import logica.ExcepcionPersonalizada;

public class Principal {

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			Cuenta c1 = new Cuenta(1, 1000, "Ahorros");
			int valorConsignacion = leerDato(br);
			c1.consignar(valorConsignacion);
			int valorRetiro = leerDato(br);
			c1.retirar(valorRetiro);
			
			Cuenta c2 = new Cuenta(2);
			int valorTransferencia = leerDato(br);
			c1.transferir(c2, valorTransferencia);
			
			br.close();
		} catch (ExcepcionPersonalizada e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static int leerDato(BufferedReader br) {
		int dato = 0;
		String datoString = ""; 
		try {
			datoString = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		dato = Integer.parseInt(datoString);
		return dato;
	}


}
